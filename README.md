# OpenAPI

Enhance your python projects with these free to use APIs! Licensed under Apache 2.0.

## What is OpenAPI?

OpenAPI is a collection of Python APIs to accomplish tasks like OS and feature detection and easy terminal resizing amongst other things. It can be useful for making projects cross-compatible with other operating systems and more! The feature set will continue to evolve as time goes on, so watch this space.

## Getting started with OpenAPI

Choose your branch. The 'edge' branch gets the latest API versions as they become available. The 'quarterly' branch hosts
zip files with APIs packaged together for optimum compatibility. As the name suggests, these files are updated quarterly and old quarterly updates are moved to their own branch and preserved for future reference. 

## More resources

Check the integration guide (when available) to see how to integrate OpenAPI into your projects properly.

The (official) system requirements for OpenAPI (latest versions) are:

* Python 3.9.0 or later.
* Windows 8.1 and later, OS-X 10.9.5 and later, Ubuntu 18.04 and later, Mint 20.2 and later, Fedora 34 and later, Debian 11 and later.
* Processor architectures: x86, x86_64, arm64.

Systems that fall outside of these specifications may work, or may not - but Python 3.9 is required.



